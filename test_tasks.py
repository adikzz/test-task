import pandas as pd
import pymongo
from pyexcelerate import Workbook

data = {
    "Id": [1, 2, 3, 4, 5, 6, 7],
    "Name": ["Alex", "Justin", "Set", "Carlos", "Gareth", "John", "Bob"],
    "Surname": ["Smur", "Forman", "Carey", "Carey", "Chapman", "James", "James"],
    "Age": [21, 25, 35, 40, 19, 27, 25],
    "Job": ["Python Developer", "Java Developer", "Project Manager", "Enterprise architect", "Python Developer", "IOS Developer", "Python Developer"],
    "Datetime": ["2022-01-01T09:45:12", "2022-01-01T11:50:25", "2022-01-01T10:00:45", "2022-01-01T09:07:36", "2022-01-01T11:54:10", "2022-01-01T09:56:40", "2022-01-01T09:52:45"],
} 

wb1 = Workbook()
wb2 = Workbook()
wb3 = Workbook()

client = pymongo.MongoClient("localhost", 27017)
db_18_21 = client.test_task.get_collection("18MoreAnd21andLess")
db_35_and_more = client.test_task.get_collection("35AndMore")
db_architect = client.test_task.get_collection("ArchitectEnterTime")

df_main = pd.DataFrame(data)

print(df_main.dtypes)
df_main["Datetime"] = pd.to_datetime(df_main["Datetime"]).dt.strftime("%Y/%m/%d %H:%M:%S")
print(df_main.dtypes)

#first conidition
df_first_task = df_main.copy()
df_first_task.loc[(df_first_task['Job'].str.contains("developer", case=False)) 
                    & (df_first_task["Age"] > 18) 
                    & (df_first_task["Age"] <= 21),
                    'TimeToEnter'] = '9.00'
df_first_task.loc[df_first_task["TimeToEnter"].isnull(), "TimeToEnter"] = "9.15" 
print(df_first_task)
db_18_21.insert_many(df_first_task.to_dict('records'))
values_first_task = [df_first_task.columns] + list(df_first_task.values)
wb1.new_sheet('sheet name', data=values_first_task)
wb1.save('first_task.xlsx')

#second conidition
df_second_task = df_main.copy()
df_second_task.loc[~(df_second_task['Job'].str.contains("developer", case=False)) 
                    & ~(df_second_task['Job'].str.contains("manager", case=False)) 
                    & (df_second_task["Age"] >= 35),
                    'TimeToEnter'] = '11.00'
df_second_task.loc[df_second_task["TimeToEnter"].isnull(), "TimeToEnter"] = "11.30" 
print(df_second_task)
db_35_and_more.insert_many(df_second_task.to_dict('records'))
values_second_task = [df_second_task.columns] + list(df_second_task.values)
wb2.new_sheet('sheet name', data=values_second_task)
wb2.save('second_task.xlsx')

#third conidition
df_third_task = df_main.copy()
df_third_task.loc[(df_third_task['Job'].str.contains("architect", case=False)),
                    'TimeToEnter'] = '10.30'
df_third_task.loc[df_third_task["TimeToEnter"].isnull(), "TimeToEnter"] = "10.40" 
print(df_third_task)
db_architect.insert_many(df_third_task.to_dict('records'))
values_third_task = [df_third_task.columns] + list(df_third_task.values)
wb3.new_sheet('sheet name', data=values_third_task)
wb3.save('third_task.xlsx')